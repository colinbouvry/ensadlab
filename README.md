#ensadlab_slam#

##Introduction##
Package ROS avec SLAM hector_mapping, gmapping et amcl.

##Installation##

Installer ROS kinetik  
http://wiki.ros.org/kinetic/Installation/Ubuntu  
en utilisant de préférence la commande suivante :  
> sudo apt-get install ros-kinetic-desktop-full
  
Installer catkin :  
http://wiki.ros.org/fr/ROS/Tutorials/InstallingandConfiguringROSEnvironment  
  
Cloner le package ROS ensadlab_slam comme ceci : catkin_ws/src/ensadlab_slam/  
  
Ajouter three.js dans le répertoire suivant : ensadlab_slam/html/  
https://threejs.org/  