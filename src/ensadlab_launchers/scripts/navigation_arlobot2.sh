#!/bin/bash
# Launch navigation_arlobot on the command line
pkill -f ros
source /opt/ros/kinetic/setup.bash
source ~/catkin_ws/devel/setup.bash
/usr/bin/python /opt/ros/kinetic/bin/roslaunch ensadlab_launchers navigation_arlobot2.launch
