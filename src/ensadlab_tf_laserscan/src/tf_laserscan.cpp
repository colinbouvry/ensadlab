#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <math.h>
#include <sstream>
#include <time.h>
#include <geometry_msgs/PointStamped.h>

std::string target_frame = "base_link";
std::string source_topic = "scan";
std::string output_topic = "base_scan";
std::string source_frame = "primesense1_depth_frame";

ros::Publisher scan_pub;

struct polar_point {
    float r;
    float theta;
};

void polar_to_tf_point(polar_point& p_point, 
    std::string frame_id, tf::Stamped<tf::Point>& st_point)
{
    float x,y,z;
    x = p_point.r*cos(p_point.theta);
    y = p_point.r*sin(p_point.theta);
    z = 0;
    const tf::Point point = tf::Point(x,y,z);
    st_point = tf::Stamped<tf::Point>(point, ros::Time::now(), frame_id);
}

void st_point_to_polar_point(tf::Stamped<tf::Point>& st_point, polar_point& point)
{
    float x = st_point.getX();
    float y = st_point.getY();
    float r = pow((pow(x,2)+pow(y,2)),0.5);
    float theta = atan2(y,x);
    point.r = r;
    point.theta = theta;
}

void callback(const sensor_msgs::LaserScan& original_msg)
{
    //if(source_frame.compare(original_msg.header.frame_id) != 0)
    //{
    //    return;
    //} 
    float o_t_min, o_t_max, o_t_inc;
    o_t_min = original_msg.angle_min;
    o_t_max = original_msg.angle_max;
    o_t_inc = original_msg.angle_increment;
    int num_points = (int)2.0*o_t_max/o_t_inc;
    sensor_msgs::LaserScan new_msg;
    tf::TransformListener transformer;

    for(int i=0; i<num_points; i++)
    {
        float theta = o_t_min+i*o_t_inc;
        float r = original_msg.ranges[i];
        polar_point point;
        point.r = r;
        point.theta = theta;
        tf::Stamped<tf::Point> old_point;
        polar_to_tf_point(point, original_msg.header.frame_id, old_point);
        tf::Stamped<tf::Point> st_point;
        geometry_msgs::PointStamped old_g_point;
        geometry_msgs::PointStamped st_g_point;
        tf::pointStampedTFToMsg(old_point, old_g_point);
        tf::pointStampedTFToMsg(st_point, st_g_point);
        try{
            transformer.lookupTransform(target_frame, // transformPoint
                 old_g_point, st_g_point);
        }
        catch(tf::TransformException ex)
        {
			ROS_ERROR("TransformPoint : %s", ex.what());
            break;
            //continue;
        }
        tf::pointStampedMsgToTF(st_g_point, st_point);
        st_point_to_polar_point(st_point, point);
        new_msg.ranges[i] = point.r;
        ROS_INFO("Range : %f ", point.r);
        if(i == 0)
        {
            new_msg.angle_min = point.theta;
        }
        else if(i == num_points - 1)
        {
            new_msg.angle_max = point.theta;
        }
    }

    new_msg.header = original_msg.header;
    new_msg.header.frame_id = target_frame;
    new_msg.angle_increment = original_msg.angle_increment;
    new_msg.time_increment = original_msg.time_increment;
    new_msg.scan_time = original_msg.scan_time;
    new_msg.intensities = original_msg.intensities;
    scan_pub.publish(new_msg);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "tf_laserscan_node");
    ros::NodeHandle n("~");

	if (n.getParam("target_frame", target_frame))
	   ROS_INFO("Got param: %s", target_frame.c_str());
	else
	   ROS_ERROR("Failed to get param 'target_frame'");

	if (n.getParam("source_topic", source_topic))
	   ROS_INFO("Got param: %s", source_topic.c_str());
	else
	   ROS_ERROR("Failed to get param 'source_topic'");
	   
	if (n.getParam("output_topic", output_topic))
	   ROS_INFO("Got param: %s", output_topic.c_str());
	else
	   ROS_ERROR("Failed to get param 'output_topic'");
	   
	if (n.getParam("source_frame", source_frame))
	   ROS_INFO("Got param: %s", source_frame.c_str());
	else
	   ROS_ERROR("Failed to get param 'source_frame'");   
   
   
    /*if(ros::param::get("target_frame", target_frame) && 
        ros::param::get("source_frame", source_frame) && 
            ros::param::get("source_topic", source_topic) &&
                ros::param::get("output_topic", output_topic))
    {
    }   
    else
    {
        ROS_INFO("laserscan_transform error!!!!");
        ROS_INFO(target_frame.c_str());
        ROS_INFO(source_frame.c_str());
        ROS_INFO(source_topic.c_str());
        ROS_INFO(output_topic.c_str());
        return 1;
    }*/ 

    ros::Subscriber sub = n.subscribe(source_topic, 1, callback);
    scan_pub = n.advertise<sensor_msgs::LaserScan>(output_topic, 1);

    ros::spin();

    return 0;
}
